# telegram-robot

This is a simple bot made to work with Telegram.
You need to add telegram token and gitlab token to the yaml file 
for the bot to function. Note also that you need to change the name
of the yaml file to "telegram-robot.yaml"

The systemd folder contains a script for running the bot as a service
in systemd. It needs to be copied to /etc/systemd/system/ .
You also need to edit the file and change the path for the ExecStart
variable.

