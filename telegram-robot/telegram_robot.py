#!/usr/bin/python3
""" Small Hello World Telegram bot """
import subprocess
from datetime import datetime
from shutil import which
from os import path
import gitlab
import yaml
from telegram import Update
from telegram.ext import Updater, CommandHandler, CallbackContext, MessageHandler, Filters


def project(update, context):
    """ Prints projects info to telegram """
    project_url = f"Gitlab: {data['project_url']}"
    update.message.reply_text(project_url)


def help_commands(update, context):
    """ Displays available commands to telegram """
    help_commands = "Commands available :\n "
    commands = ["help", "fortune", "latest_commit", "project", "date"]
    for item in commands:
        help_commands += item + "\n"
    update.message.reply_text(help_commands)


def fortune(update, context):
    """ Prints a fortune, if installed, to telegram """
    if which('fortune'):
        fortune = subprocess.getoutput('fortune')
        update.message.reply_text(fortune)
    else:
        update.message.reply_text('Sorry, fortune not installd')


def latest_commit(update, context):
    """ Prints the latest commit message to telegram """
    git_lab = gitlab.Gitlab('https://gitlab.com', private_token=data['gitlab_token'])
    project = git_lab.projects.get(data['git_project_id'])
    commits = project.commits.list()
    message = f"Commit message: {commits[0].message}Committer email: {commits[0].committer_email}"
    update.message.reply_text(message)


def unknown(update, context):
    """Error handling for unkown commands"""
    context.bot.send_message(chat_id=update.effective_chat.id, text="Sorry, i dont understand that, try /help")


def read_yaml():
    """Reads a YAML file"""
    basepath = path.dirname(__file__)
    filepath = path.abspath(path.join(basepath, "telegram-robot.yaml"))
    with open(filepath, 'r') as file:
        data = yaml.load(file, Loader=yaml.FullLoader)
    return data


def date(update, context):
    """Prints the current date"""
    context.bot.send_message(chat_id=update.effective_chat.id, text=datetime.today().strftime('%Y-%m-%d'))


def main():
    """ Initialises and runs telegram bot """
    updater = Updater(data['telegram_token'], use_context=True)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('latest_commit', latest_commit))
    dispatcher.add_handler(CommandHandler('fortune', fortune))
    dispatcher.add_handler(CommandHandler('help', help_commands))
    dispatcher.add_handler(CommandHandler('project', project))
    dispatcher.add_handler(CommandHandler('date', date))
    unknown_handler = MessageHandler(Filters.command, unknown)
    dispatcher.add_handler(unknown_handler)
    updater.start_polling()
    updater.idle()


data = read_yaml()

if __name__ == '__main__':
    main()
